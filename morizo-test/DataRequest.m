//
//  DataRequest.m
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "DataRequest.h"
#import "ElementOfDataMap.h"
#import "ElementsAirport.h"
#import "ElementsShop.h"
#import "LocalUniversalCollectionViewController.h"

@implementation DataRequest

+(NSMutableArray *) convertJsonToPresentDataMap{
    
    NSString *pathName = @"map";
    ElementOfDataMap *data = [ElementOfDataMap alloc];
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:pathName ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSData *dataJson = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataJson options:0 error:nil];
    
    NSArray *names = [json objectForKey:@"nameRu"];
    NSArray *codes = [json objectForKey:@"codeRu"];
    NSArray *type = [json objectForKey:@"type"];
    
    [data setNameRu:[[names copy] autorelease]];
    [data setCodeRu:[[codes copy] autorelease]];
    [data setType:[[type copy] autorelease]];

    [dataArray addObject: data];
  
    return dataArray;
}


+(NSMutableArray *) convertJsonToPresentDataAirfield{
    
    NSString *pathName         = @"Airfield";
    ElementsAirport *data      = [ElementsAirport alloc];
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    
    NSString *filePath   = [[NSBundle mainBundle] pathForResource:pathName ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSData *dataJson     = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json   = [NSJSONSerialization JSONObjectWithData:dataJson options:0 error:nil];
    
    NSArray *names  = [json objectForKey:@"nameRu"];
    NSArray *cities = [json objectForKey:@"cityRu"];
    NSArray *type   = [json objectForKey:@"type"];
    
    [data setNameRu:[[names copy] autorelease]];
    [data setCityRu:[[cities copy] autorelease]];
    [data setType:[[type copy] autorelease]];
    
    [dataArray addObject: data];
    
    return dataArray;
}


+(NSMutableArray *) convertJsonToPresentDataShop {
    
    NSString *pathName         = @"shop";
    ElementsShop *data      = [ElementsShop alloc];
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    
    NSString *filePath   = [[NSBundle mainBundle] pathForResource:pathName ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSData *dataJson     = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json   = [NSJSONSerialization JSONObjectWithData:dataJson options:0 error:nil];
    
    NSArray *names  = [json objectForKey:@"nameRu"];
    //NSArray *cities = [json objectForKey:@"cityRu"];
    //NSArray *type   = [json objectForKey:@"type"];
    
    [data setNameRu:[[names copy] autorelease]];
    //[data setCityRu:[[cities copy] autorelease]];
    //[data setType:[[type copy] autorelease]];
    
    [dataArray addObject: data];
    return dataArray;
}


@end
