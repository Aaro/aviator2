//
//  MapsCollectionViewController.m
//  morizo-test
//
//  Created by Nadezhda on 13.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "MapsCollectionViewController.h"
#import "DataRequest.h"

@interface MapsCollectionViewController ()

@end

@implementation MapsCollectionViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [DataRequest convertJsonToPresentDataMap];
    self.aviaControllerType = 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
