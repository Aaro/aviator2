//
//  UniversalDataClass.m
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "LocalUniversalCollectionViewController.h"
#import "DataCell.h"
#import "ElementOfDataMap.h"
#import "ElementsAirport.h"
#import "ElementsShop.h"


@interface LocalUniversalCollectionViewController ()


@end

@implementation LocalUniversalCollectionViewController

static NSString * const reuseIdentifier = @"Cell";


-(void)setUniversalDataArray:(NSArray *)data{
    self.universalDataArray = data;
    //self.universalDataArray.city;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor = [UIColor redColor];
    [self.collectionView registerClass:DataCell.class forCellWithReuseIdentifier:reuseIdentifier];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>
/*
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete implementation, return the number of sections
    return 0;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of items
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    return cell;
}
*/
#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(250, 200);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataArray count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DataCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
    ElementOfDataMap *dataForCell = [ElementOfDataMap alloc];
    dataForCell = self.dataArray[indexPath.row];
    
    if (self.aviaControllerType == 0){
        ElementOfDataMap *dataForCell = [ElementOfDataMap alloc];
        dataForCell = self.dataArray[indexPath.row];
        
        [cell addLabel:dataForCell.nameRu: 10 :10 :100 :100];
        [cell addLabel:dataForCell.codeRu: 10 :30 :100 :100];
        [cell addLabel:dataForCell.type: 10 :50 :100 :100];
    }
    
    if (self.aviaControllerType == 1){
        ElementsAirport *dataForCell = [ElementsAirport alloc];
        dataForCell = self.dataArray[indexPath.row];
        
        [cell addLabel:dataForCell.nameRu: 10 :10 :100 :100];
        [cell addLabel:dataForCell.cityRu: 10 :30 :100 :100];
        [cell addLabel:dataForCell.type: 10 :50 :100 :100];
    }
    
    if (self.aviaControllerType == 2){
        ElementsShop*dataForCell = [ElementsShop alloc];
        dataForCell = self.dataArray[indexPath.row];
        [cell addLabel:dataForCell.nameRu: 10 :10 :100 :100];
    }
   
 return cell;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


@end
