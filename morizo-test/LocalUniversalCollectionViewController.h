//
//  UniversalDataClass.h
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainVC.h"

typedef NS_ENUM(NSUInteger, AviaControllerType) {
    AviaControllerTypeMap = 0,
    AviaControllerTypeAir = 1,
    AviaControllerTypeShop = 2
};



@interface LocalUniversalCollectionViewController : UICollectionViewController

@property AviaControllerType aviaControllerType;
@property(strong, nonatomic)    NSArray *dataArray;

@end
