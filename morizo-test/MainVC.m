//
//  MainVC.m
//  morizo-test
//
//  Created by Nadezhda on 27.06.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "MainVC.h"
#import "LocalUniversalCollectionViewController.h"



@implementation MainVC


-(void) viewDidLoad{
    [super viewDidLoad];
}


-(NSString *) segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath{
    NSString* identifier = nil;
    
    if (indexPath.section==1){
        switch (indexPath.row) {
            case 0:
                identifier = @"mapSegue";
                //делаем запрос для карт
                //cities = @[@"1", @"2", @"3"];
                //countries = @[@"10", @"20", @"30"];
                //utr = @[@"1-1", @"2-2", @"3-3"];
                //wht = @[@"10-1", @"20-2", @"30-3"];
               // [DataRequest convertJsonToPresentData: indexPath.row];
                break;
            
            case 1:
                identifier = @"airportSegue";
                //делаем запрос для аэропортов
               // [DataRequest convertJsonToPresentDataMap:indexPath.row];
                break;
            
            case 2:
                identifier = @"shopSegue";
                //делаем запрос для магазина
                break;
                
            case 3:
                identifier = @"updatingSegue";
                // делаем запрос для обновлений
                break;
                
            case 4:
                identifier = @"settingsSegue";
                break;
                
            case 5:
                identifier = @"spavkaSegue";
                break;
        }
    }
    else{
        switch (indexPath.row) {
            case 0:
                identifier = @"nameSegue";
                break;
                
            case 1:
                identifier = @"exitSegue";
                break;
        }
    }
    return identifier;
}


-(void)configureLeftMenuButton:(UIButton *)button{
    CGRect frame  = button.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.height = 40;
    frame.size.width = 40;
    button.frame = frame;
    
    [button setImage:[UIImage imageNamed:@"menu"] forState: UIControlStateNormal];
}


-(BOOL)deepnessForLeftMenu{
    return YES;
}


//-(CGFloat)leftMenuWidth{
//    return 200;
//}


@end
