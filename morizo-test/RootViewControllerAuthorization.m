//
//  RootViewControllerAuthorization.m
//  morizo-test
//
//  Created by Nadezhda on 29.06.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "RootViewControllerAuthorization.h"
#import "MainVC.h"
#import "RegistrationViewController.h"

@interface RootViewControllerAuthorization ()

@end

@implementation RootViewControllerAuthorization

- (void)viewDidLoad {
    [super viewDidLoad];
    //labelForgotPassword.text = [labelForgotPassword.text addAttribute:(NSString*)kCTUnderlineStyleAttributeName
    //                                             value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]]
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)AuthorizationPressed:(UIButton *)sender{
    MainVC * mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"mainVC"];
    [self presentViewController:mainVC animated:YES completion:nil];
}


-(IBAction)RegistrationPressed:(UIButton *)sender{
    RegistrationViewController * registration = [self.storyboard instantiateViewControllerWithIdentifier:@"registration"];
    [self presentViewController:registration animated:YES completion:nil];
}



@end
