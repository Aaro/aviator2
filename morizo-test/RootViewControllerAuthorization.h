//
//  RootViewControllerAuthorization.h
//  morizo-test
//
//  Created by Nadezhda on 29.06.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RootViewControllerAuthorization : UIViewController{
    IBOutlet UILabel *labelForgotPassword;
}
-(IBAction)AuthorizationPressed:(UIButton *)sender;
-(IBAction)RegistrationPressed:(UIButton *)sender;

@end
