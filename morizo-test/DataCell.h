//
//  DataCell.h
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataCell : UICollectionViewCell {
    
}
@property (retain, nonatomic) IBOutlet UILabel *cityLabel;

@property (retain, nonatomic) IBOutlet UILabel *countryLabel;
@property (retain, nonatomic) IBOutlet UILabel *svoLabel;
@property (retain, nonatomic) IBOutlet UILabel *utrLabel;

@property (retain, nonatomic) IBOutlet UILabel *testLabel;




-(void)addLabel:(NSString *)text :(CGFloat)x :(CGFloat)y :(CGFloat)width :(CGFloat) height;

@end
