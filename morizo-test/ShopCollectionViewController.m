//
//  ShopCollectionViewController.m
//  morizo-test
//
//  Created by Nadezhda on 15.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "ShopCollectionViewController.h"
#import "DataRequest.h"

@interface ShopCollectionViewController ()

@end

@implementation ShopCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [DataRequest convertJsonToPresentDataShop];
    self.aviaControllerType = 2;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
