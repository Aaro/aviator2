//
//  ElementsShop.h
//  morizo-test
//
//  Created by Nadezhda on 15.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ElementsShop : NSObject

@property (nonatomic, retain)     NSString *shopId;
@property (nonatomic, retain)     NSString *nameRu;
@property (nonatomic, retain)     NSString *nameEn;
@property (nonatomic, retain)     NSString *priceQuater;
@property (nonatomic, retain)     NSString *priceHalf;
@property (nonatomic, retain)     NSString *priceYear;
@property (nonatomic, retain)     NSString *maps;
@property (nonatomic, retain)     NSString *changed;
@property (nonatomic, retain)     NSString *fileChanged;
@property (nonatomic, retain)     NSString *fileId;
@property (nonatomic, retain)     NSString *fileName;
@property (nonatomic, retain)     NSString *fileUrl;
@property (nonatomic, retain)     NSString *fileSize;


@end
