//
//  AirportsCollectionViewController.m
//  morizo-test
//
//  Created by Nadezhda on 15.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "AirportsCollectionViewController.h"
#import "DataRequest.h"

@interface AirportsCollectionViewController ()

@end

@implementation AirportsCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [DataRequest convertJsonToPresentDataAirfield];
    self.aviaControllerType = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
