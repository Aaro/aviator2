//
//  elementOfData.h
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ElementOfDataMap : NSObject

@property (nonatomic, retain)     NSString *idMap;
@property (nonatomic, retain)     NSString *nameRu;
@property (nonatomic, retain)     NSString *nameEn;
@property (nonatomic, retain)     NSString *codeRu;
@property (nonatomic, retain)     NSString *codeEn;
@property (nonatomic, retain)     NSString *beginDate;
    
@property (nonatomic, retain)     NSString *priceQuater;
@property (nonatomic, retain)     NSString *priceHalf;
@property (nonatomic, retain)     NSString *priceYear;
@property (nonatomic, retain)     NSString *type;
@property (nonatomic, retain)     NSString *changed;
@property (nonatomic, retain)     NSString *fileChanged;
    
@property (nonatomic, retain)     NSString *previewId;
@property (nonatomic, retain)     NSString *previewFileName;
@property (nonatomic, retain)     NSString *previewFileUrl;
@property (nonatomic, retain)     NSString *previewFileSize;
    
@property (nonatomic, retain)     NSString *coordinatesLat;
@property (nonatomic, retain)     NSString *coordinatesLong;
        
@end
