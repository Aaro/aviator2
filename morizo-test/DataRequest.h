//
//  DataRequest.h
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataRequest : NSObject
{
    NSArray *jsonData;
    NSArray *presentData;

}

+(NSMutableArray *) convertJsonToPresentDataMap;
+(NSMutableArray *) convertJsonToPresentDataAirfield;
+(NSMutableArray *) convertJsonToPresentDataShop;

@end
