//
//  AppDelegate.h
//  morizo-test
//
//  Created by Nadezhda on 27.06.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LargeImageDownsizingViewController.h"

@class LargeImageDownsizingViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate>{
}//   //UIResponder

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) IBOutlet LargeImageDownsizingViewController *viewController;



@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end


