//
//  main.m
//  morizo-test
//
//  Created by Nadezhda on 27.06.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [pool release];
//    int retVal = UIApplicationMain(argc, argv, nil, nil);
//    return retVal;
    @autoreleasepool {
     return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
