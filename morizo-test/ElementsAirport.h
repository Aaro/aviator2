//
//  ElementsAirport.h
//  morizo-test
//
//  Created by Nadezhda on 15.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ElementsAirport : NSObject

 @property (nonatomic, retain)   NSString *airId;
 @property (nonatomic, retain)   NSString *nameRu;
 @property (nonatomic, retain)   NSString *nameEn;
 @property (nonatomic, retain)   NSString *cityRu;
 @property (nonatomic, retain)   NSString *cityEn;
 @property (nonatomic, retain)   NSString *codeIATA;
 @property (nonatomic, retain)   NSString *codeIATAEn;
 @property (nonatomic, retain)   NSString *codeAD;
 @property (nonatomic, retain)   NSString *codeADEn;
 @property (nonatomic, retain)   NSString *coordLat;
 @property (nonatomic, retain)   NSString *coordLong;
 @property (nonatomic, retain)   NSString *latBin;
 @property (nonatomic, retain)   NSString *longBin;
 @property (nonatomic, retain)   NSString *type;
 @property (nonatomic, retain)   NSString *ved;
 @property (nonatomic, retain)   NSString *timezone;
 @property (nonatomic, retain)   NSString *changed;
 @property (nonatomic, retain)   NSString *fileChanged;
 @property (nonatomic, retain)   NSString *schemesId;
 @property (nonatomic, retain)   NSString *schemesNameRu;
 @property (nonatomic, retain)   NSString *schemesType;


@end
