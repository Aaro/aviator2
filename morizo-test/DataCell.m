//
//  DataCell.m
//  morizo-test
//
//  Created by Nadezhda on 08.07.16.
//  Copyright © 2016 Nadezhda. All rights reserved.
//

#import "DataCell.h"

@implementation DataCell


- (void)dealloc {
    [_cityLabel release];
    [_countryLabel release];
    [_svoLabel release];
    [_utrLabel release];
    [super dealloc];
}


-(void)addLabel:(NSString *)text :(CGFloat)x :(CGFloat)y :(CGFloat)width :(CGFloat) height{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x , y, width, height)];
    label.text = text;
    [label setTextColor:[UIColor whiteColor]];
    [self addSubview:label];
}


@end
